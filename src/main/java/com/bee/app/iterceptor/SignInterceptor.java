package com.bee.app.iterceptor;

import java.util.HashMap;
import java.util.Map;

import com.bee.app.config.CODE;
import com.bee.app.sign.SignKit;
import com.bee.app.vo.BaseVo;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.log.Logger;

/**
 * 签名拦截器
 * 
 * @author liuzy
 * @version 2015年7月5日
 */
public class SignInterceptor implements Interceptor {

	private Logger logger = Logger.getLogger(SignInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		Map<String, String> params = convert(controller.getParaMap());
		logger.info("[参数]" + params);
		if (SignKit.isSign(params)) {
			logger.info("[签名通过]");
			inv.invoke();
		} else {
			logger.info("[签名错误]");
			controller.renderJson(new BaseVo(CODE.E999_MSG).signed());
		}
	}

	private Map<String, String> convert(Map<String, String[]> map) {
		Map<String, String> params = new HashMap<String, String>();
		for (String key : map.keySet()) {
			params.put(key, map.get(key)[0]);
		}
		return params;
	}
	
}
