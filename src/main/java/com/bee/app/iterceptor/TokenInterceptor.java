package com.bee.app.iterceptor;

import com.bee.app.config.CODE;
import com.bee.app.token.TokenManager;
import com.bee.app.vo.BaseVo;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.log.Logger;

/**
 * 令牌拦截器
 * 
 * @author liuzy
 * @version 2015年7月5日
 */
public class TokenInterceptor implements Interceptor {

	private Logger logger = Logger.getLogger(TokenInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		String token = controller.getPara("token");
		if (TokenManager.isToken(token)) {
			logger.info("[令牌有效]");
			inv.invoke();
		} else {
			logger.info("[令牌无效]");
			controller.renderJson(new BaseVo(CODE.E005_MSG).signed());
		}
	}

}
