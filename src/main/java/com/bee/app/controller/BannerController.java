package com.bee.app.controller;

import com.bee.app.iterceptor.TokenInterceptor;
import com.bee.app.jfinal.Controller;
import com.bee.app.service.BannerService;
import com.bee.app.vo.BannerVo;
import com.jfinal.aop.Before;

/**
 * 
 * @author liuzy
 * @version 2015年7月3日
 */
@Controller("/banner")
@Before(TokenInterceptor.class)
public class BannerController extends BaseController {

	public void list() {
		BannerVo vo = new BannerVo();
		vo.setBannerList(BannerService.service.listAll());
		renderFastJson(vo);
	}

}
