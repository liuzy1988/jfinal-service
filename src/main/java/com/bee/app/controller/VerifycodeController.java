package com.bee.app.controller;

import com.bee.app.jfinal.Controller;
import com.bee.app.sms.SmsManager;
import com.bee.app.vo.VerifyCodeVo;

/**
 * 
 * @author liuzy
 * @version 2015年7月3日
 */
@Controller("/verifycode")
public class VerifycodeController extends BaseController {

	/**
	 * 发送验证码
	 */
	public void resend() {
		String phoneNumber = getPara("phoneNumber");
		String smsId = SmsManager.send(phoneNumber);
		vo = new VerifyCodeVo(smsId);
		renderFastJson(vo);
	}

}
