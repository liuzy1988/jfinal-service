package com.bee.app.controller;

import com.bee.app.iterceptor.TokenInterceptor;
import com.bee.app.jfinal.Controller;
import com.bee.app.service.ExpressService;
import com.bee.app.token.TokenManager;
import com.jfinal.aop.Before;

/**
 * 
 * @author liuzy
 * @version 2015年7月3日
 */
@Controller("/express")
public class ExpressController extends BaseController {

	@Before(TokenInterceptor.class)
	public void query() {
		String token = getPara("token");
		String expressNumber = getPara("expressNumber");
		String userId = TokenManager.token2userId(token);
		renderFastJson(ExpressService.service.query(userId, expressNumber));
	}

}
