package com.bee.app.controller;

import com.bee.app.jfinal.Controller;

/**
 * 
 * @author liuzy
 * @version 2015年7月2日
 */
@Controller("/pay")
public class PayController extends BaseController {
	public void cash() {
		renderJson("cash");
	}

	public void request() {
		renderJson("request");
	}

	public void result() {
		renderJson("result");
	}
}
