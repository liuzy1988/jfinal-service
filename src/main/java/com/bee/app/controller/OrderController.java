package com.bee.app.controller;

import com.bee.app.jfinal.Controller;

/**
 * 
 * @author liuzy
 * @version 2015年7月2日
 */
@Controller("/order")
public class OrderController extends BaseController {
	public void create() {
		renderJson("create");
	}

	public void receive() {
		renderJson("receive");
	}

	public void cancel() {
		renderJson("cancel");
	}

	public void update() {
		renderJson("update");
	}

	public void list() {
		renderJson("list");
	}
}
