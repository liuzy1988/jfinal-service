package com.bee.app.controller;

import com.bee.app.config.CODE;
import com.bee.app.iterceptor.TokenInterceptor;
import com.bee.app.jfinal.Controller;
import com.bee.app.model.User;
import com.bee.app.service.UserService;
import com.bee.app.sms.SmsManager;
import com.bee.app.token.TokenManager;
import com.bee.app.util.StringKit;
import com.bee.app.vo.BaseVo;
import com.bee.app.vo.UserInfoVo;
import com.bee.app.vo.UserLoginVo;
import com.jfinal.aop.Before;

/**
 * 
 * @author liuzy
 * @version 2015年7月2日
 */
@Controller("/user")
public class UserController extends BaseController {

	/**
	 * 检查用户是否在在
	 */
	public void check() {
		String userName = getPara("userName");
		if (StringKit.isNullOrEmpty(userName)) {
			renderFastJson(CODE.E000_MSG);
		} else {
			boolean flag = StringKit.isPhoneNumber(userName);
			if (flag && UserService.service.checkPhoneNumber(userName)) {
				renderFastJson(CODE.E002_MSG);
			} else if (!flag && UserService.service.checkUserName(userName)) {
				renderFastJson(CODE.E001_MSG);
			} else {
				renderFastJson(vo);
			}
		}
	}

	/**
	 * 用户注册
	 */
	public void register() {
		String userName = getPara("userName");
		String password = getPara("password");
		String verifyCode = getPara("verifyCode");
		String smsId = getPara("smsId");
		if (StringKit.hasNullOrEmpty(userName, password, verifyCode, smsId)) {
			renderFastJson(CODE.E000_MSG);
		} else {
			boolean isPhoneNumber = StringKit.isPhoneNumber(userName);
			if (isPhoneNumber && UserService.service.checkPhoneNumber(userName)) {
				renderFastJson(CODE.E002_MSG);
			} else if (!isPhoneNumber && UserService.service.checkUserName(userName)) {
				renderFastJson(CODE.E001_MSG);
			} else {
				if (!SmsManager.validate(smsId, verifyCode)) {
					renderFastJson(CODE.E003_MSG);
				} else {
					if (UserService.service.register(userName, password, isPhoneNumber)) {
						renderFastJson(vo);
					} else {
						renderFastJson(new BaseVo("fail", "注册失败"));
					}
				}
			}
		}
	}

	/**
	 * 登陆
	 */
	public void login() {
		String userName = getPara("userName");
		String password = getPara("password");
		boolean isPhoneNumber = StringKit.isPhoneNumber(userName);
		User user = UserService.service.validate(userName, password, isPhoneNumber);
		if (user != null) {
			UserLoginVo vo = new UserLoginVo();
			String userId = user.getStr("userId");
			vo.setUserId(userId);
			vo.setToken(TokenManager.createToken(userId));
			vo.setAreaId(user.getStr("areaId"));
			renderFastJson(vo);
		} else {
			renderFastJson(CODE.E004_MSG);
		}
	}

	/**
	 * 注销
	 */
	@Before(TokenInterceptor.class)
	public void logout() {
		renderJson("logout");
	}

	/**
	 * 获取用户信息
	 */
	@Before(TokenInterceptor.class)
	public void info() {
		String userName = getPara("userName");
		User user = UserService.service.getUserInfo(userName);
		UserInfoVo vo = new UserInfoVo();
		vo.setUserId(user.getStr("userId"));
		vo.setUserName(user.getStr("userName"));
		vo.setUserType(user.getStr("userType"));
		vo.setPhoneNumber(user.getStr("phoneNumber"));
		vo.setEmail(user.getStr("email"));
		vo.setScore(user.getInt("score"));
		vo.setPhoto(user.getStr("sphoto"));
		renderFastJson(vo);
	}

	/**
	 * 修改用户信息
	 */
	@Before(TokenInterceptor.class)
	public void uinfo() {
		String token = getPara("token");
		String email = getPara("email");
		String photo = getPara("photo");
		String userId = TokenManager.token2userId(token);
		if (UserService.service.updateUserInfo(userId, email, photo)) {
			renderFastJson(vo);
		} else {
			renderFastJson(CODE.ERROR_MSG);
		}
	}

	/**
	 * 修改密码
	 */
	@Before(TokenInterceptor.class)
	public void upassword() {
		String token = getPara("token");
		String oldPassword = getPara("oldPassword");
		String newPassword = getPara("newPassword");
		String userId = TokenManager.token2userId(token);
		if (UserService.service.updateUserPwd(userId, oldPassword, newPassword)) {
			renderFastJson(vo);
		} else {
			renderFastJson(new BaseVo("fail", "旧密码错误"));
		}
	}
}
