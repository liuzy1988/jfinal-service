package com.bee.app.controller;

import org.apache.log4j.Logger;

import com.bee.app.iterceptor.SignInterceptor;
import com.bee.app.util.FastJsonUtil;
import com.bee.app.vo.BaseVo;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.POST;

/**
 * 接口必须POST请求，必须签名
 * 
 * @author liuzy
 * @version 2015年7月2日
 */
@Before({ POST.class, SignInterceptor.class })
public class BaseController extends Controller {

	private Logger logger = Logger.getLogger(BaseController.class);

	public BaseVo vo = new BaseVo();

	public void renderFastJson(String dictKey) {
		vo = new BaseVo(dictKey);
		renderFastJson(vo);
	}

	public void renderFastJson(BaseVo vo) {
		String json = FastJsonUtil.toJSON(vo.signed());
		logger.info("[响应]" + json);
		renderJson(json);
	}

}
