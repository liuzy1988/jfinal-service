package com.bee.app.controller;

import com.bee.app.config.CODE;
import com.bee.app.iterceptor.TokenInterceptor;
import com.bee.app.jfinal.Controller;
import com.bee.app.model.Sugesstion;
import com.bee.app.push.PushTask;
import com.bee.app.service.PublicService;
import com.bee.app.token.TokenManager;
import com.bee.app.util.RSA;
import com.bee.app.util.StringKit;
import com.bee.app.vo.PublicAreaVo;
import com.bee.app.vo.PublicUpdateVo;
import com.jfinal.aop.Before;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
@Controller("/")
public class PublicController extends BaseController {

	/**
	 * 更新
	 */
	public void update() {
		String terminalType = getPara("terminalType");
		String version = getPara("version");
		if (StringKit.hasNullOrEmpty(terminalType, version)) {
			renderFastJson(CODE.E000_MSG);
		} else {
			PublicUpdateVo vo = new PublicUpdateVo();
			vo.setLatesVesion("V0.1");
			vo.setDownloadUrl("http://www.bee.com/download/bee.apk");
			vo.setSecret(RSA.getPublicKey());
			vo.setCodeList(CODE.toList());
			vo.setCodeMap(CODE.toMap());
			renderFastJson(vo);
		}
	}

	/**
	 * 服务区域
	 */
	public void areas() {
		PublicAreaVo vo = new PublicAreaVo();
		vo.setAreaList(PublicService.service.listAll());
		renderFastJson(vo);
	}

	/**
	 * 意见反馈
	 */
	@Before(TokenInterceptor.class)
	public void sugesstion() {
		String token = getPara("token");
		String content = getPara("content");
		String userId = TokenManager.token2userId(token);
		new Sugesstion().put("userId", userId).put("content", content).save();
		renderFastJson(vo);
	}

	/**
	 * 推送测试
	 */
	public void push() {
		String userId = getPara("userId");
		String areaId = getPara("areaId");
		String title = getPara("title");
		String message = getPara("message");
		new Thread(new PushTask(userId, areaId, title, message)).start();
		renderFastJson(vo);
	}

}
