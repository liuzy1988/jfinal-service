package com.bee.app.service;

import java.util.ArrayList;
import java.util.List;

import com.bee.app.model.ServiceArea;
import com.bee.app.vo.bean.AreaBean;

/**
 * 
 * @author liuzy
 * @version 2015年7月5日
 */
public class PublicService extends BaseService {

	public static final PublicService service = new PublicService();

	public List<AreaBean> listAll() {
        List<AreaBean> list = new ArrayList<>();
		List<ServiceArea> areas = ServiceArea.dao.find("select * from bee_service_area");
		for (ServiceArea entity : areas) {
            AreaBean bean = new AreaBean();
            bean.setAreaId(entity.getStr("areaId"));
            bean.setAreaName(entity.getStr("areaName"));
            list.add(bean);
        }
		return list;
	}

}
