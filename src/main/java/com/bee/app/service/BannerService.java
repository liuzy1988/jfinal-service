package com.bee.app.service;

import java.util.ArrayList;
import java.util.List;

import com.bee.app.model.Banner;
import com.bee.app.vo.bean.BannerBean;

/**
 * 
 * @author liuzy
 * @version 2015年7月5日
 */
public class BannerService extends BaseService {

	public static final BannerService service = new BannerService();

	public List<BannerBean> listAll() {
		List<BannerBean> list = new ArrayList<>();
		List<Banner> banners = Banner.dao.find("select * from bee_banner");
		for (Banner banner : banners) {
			BannerBean bean = new BannerBean(banner.getStr("title"), banner.getStr("imgUrl"), banner.getStr("href"));
			list.add(bean);
		}
		return list;
	}

}
