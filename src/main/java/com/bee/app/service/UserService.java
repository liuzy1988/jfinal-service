package com.bee.app.service;

import com.bee.app.model.User;
import com.bee.app.tool.SqlBuilder;
import com.bee.app.util.IDUtil;
import com.bee.app.util.PwdUtil;
import com.jfinal.plugin.activerecord.Db;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
public class UserService extends BaseService {

	public static final UserService service = new UserService();// MyTxProxy.newProxy(UserService.class);

	/**
	 * 检查手机号是否在在
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public boolean checkPhoneNumber(String phoneNumber) {
		String sql = "select count(*) from bee_user where phoneNumber=?";
		return Db.queryLong(sql, phoneNumber).intValue() > 0;
	}

	/**
	 * 检查用户名是否在在
	 * 
	 * @param userName
	 * @return
	 */
	public boolean checkUserName(String userName) {
		String sql = "select count(*) from bee_user where userName=?";
		return Db.queryLong(sql, userName).intValue() > 0;
	}

	/**
	 * 用户注册
	 * 
	 * @param userName
	 * @param password
	 * @param isPhoneNumber
	 */
	public boolean register(String userName, String password, boolean isPhoneNumber) {
		String salt = PwdUtil.getSalt();
		String pwd = PwdUtil.encrypt(password, salt);
		User user = new User();
		user.put("userId", IDUtil.rand());
		if (isPhoneNumber)
			user.put("phoneNumber", userName);
		else
			user.put("userName", userName);
		user.put("salt", salt);
		user.put("password", pwd);
		return user.save();
	}

	/**
	 * 用户登陆验证
	 * 
	 * @param userName
	 * @param password
	 * @param isPhoneNumber
	 * @return
	 */
	public User validate(String userName, String password, boolean isPhoneNumber) {
		String sql = null;
		if (isPhoneNumber)
			sql = "select * from bee_user where phoneNumber=?";
		else
			sql = "select * from bee_user where userName=?";
		User user = User.dao.findFirst(sql, userName);
		if (user == null)
			return null;
		String salt = user.getStr("salt");
		String pwd = user.getStr("password");
		boolean flag = PwdUtil.validate(password, salt, pwd);
		if (flag)
			return user;
		else
			return null;
	}

	/**
	 * 获取用户信息
	 * 
	 * @param userName
	 * @return
	 */
	public User getUserInfo(String userName) {
		SqlBuilder sqlBuilder = new SqlBuilder().model(new User()).and("userName", userName);
		User user = User.dao.findFirst(sqlBuilder.sql(), sqlBuilder.values());
		return user;
	}

	public boolean updateUserInfo(String userId, String email, String photo) {
		return new User().put("userId", userId).put("email", email).put("photo", photo).update();
	}

	/**
	 * 修改密码
	 * 
	 * @param userId
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 */
	public boolean updateUserPwd(String userId, String oldPassword, String newPassword) {
		User user = User.dao.findFirst("select * from bee_user where userId=?", userId);
		if (PwdUtil.validate(oldPassword, user.getStr("salt"), user.getStr("password"))) {
			String newSalt = PwdUtil.getSalt();
			String newPwd = PwdUtil.encrypt(newPassword, newSalt);
			return user.set("salt", newSalt).set("password", newPwd).update();
		}
		return false;
	}

}
