package com.bee.app.push;

import cn.jpush.api.JPushClient;
import cn.jpush.api.common.resp.APIConnectionException;
import cn.jpush.api.common.resp.APIRequestException;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;

import com.jfinal.kit.PropKit;

/**
 * 极光推送
 *
 * @author liuzy
 * @since 2015年6月2日
 */
public class JPush {

	private static String appKey = PropKit.get("jpush.appKey");

	private static String masterSecret = PropKit.get("jpush.masterSecret");

	private static Integer maxRetryTimes = PropKit.getInt("jpush.maxRetryTimes");

	private static JPushClient jPushClient;

	private static void init() {
		if (jPushClient == null) {
			jPushClient = new JPushClient(masterSecret, appKey, maxRetryTimes);
		}
	}

    /**
     * 给所有用户推送消息
     *
     * @param message
     * @return
     */
    public static String pushAllUser(String title, String message) {
        init();
        PushPayload pushPayload = PushPayload.newBuilder()
                .setPlatform(Platform.all())
                .setAudience(Audience.all())
                .setNotification(Notification.newBuilder()
                        .setAlert(message)
                        .addPlatformNotification(AndroidNotification.newBuilder().setTitle(title).setAlert(message).build())
                        .addPlatformNotification(IosNotification.newBuilder().incrBadge(1).addExtra(title, message).build())
                        .build())
                .build();
        try {
            return jPushClient.sendPush(pushPayload).toString();
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (APIRequestException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 给指定用户推送消息
     *
     * @param userId
     * @param message
     * @return
     */
    public static String pushByUserId(String userId, String title, String message) {
        init();
        PushPayload pushPayload = PushPayload.newBuilder()
                .setPlatform(Platform.all())
                .setAudience(Audience.alias(userId))
                .setNotification(Notification.newBuilder()
                        .setAlert(message)
                        .addPlatformNotification(AndroidNotification.newBuilder().setTitle(title).setAlert(message).build())
                        .addPlatformNotification(IosNotification.newBuilder().incrBadge(1).addExtra(title, message).build())
                        .build())
                .build();
        try {
            return jPushClient.sendPush(pushPayload).toString();
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (APIRequestException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 给区域内店员推送消息
     *
     * @param areaId
     * @param title
     * @param message
     * @return
     */
    public static String pushByAreaId(String areaId, String title, String message) {
        init();
        PushPayload pushPayload = PushPayload.newBuilder()
                .setPlatform(Platform.all())
                .setAudience(Audience.tag(areaId))
                .setNotification(Notification.newBuilder()
                        .setAlert(message)
                        .addPlatformNotification(AndroidNotification.newBuilder().setTitle(title).setAlert(message).build())
                        .addPlatformNotification(IosNotification.newBuilder().incrBadge(1).addExtra(title, message).build())
                        .build())
                .build();
        try {
            return jPushClient.sendPush(pushPayload).toString();
        } catch (APIConnectionException e) {
            e.printStackTrace();
        } catch (APIRequestException e) {
            e.printStackTrace();
        }
        return null;
    }

}
