package com.bee.app.push;

import com.bee.app.util.StringKit;
import com.jfinal.log.Logger;

/**
 * 
 * @author liuzy
 * @version 2015年7月5日
 */
public class PushTask implements Runnable {

	private Logger logger = Logger.getLogger(PushTask.class);

	private String userId;
	private String areaId;
	private String title;
	private String message;

	public PushTask(String userId, String areaId, String title, String message) {
		this.userId = userId;
		this.areaId = areaId;
		this.title = title;
		this.message = message;
	}

	@Override
	public void run() {
		logger.info("推送开始......");
		if (!StringKit.isNullOrEmpty(userId)) {
			JPush.pushByUserId(userId, title, message);
		} else if (!StringKit.isNullOrEmpty(areaId)) {
			JPush.pushByAreaId(areaId, title, message);
		} else {
			JPush.pushAllUser(title, message);
		}
		logger.info("推送完成......");
	}

}
