package com.bee.app.vo;

/**
 * 发送短信验证码返回对象
 *
 * @author liuzy
 */
public class VerifyCodeVo extends BaseVo {

	private String smsId;

	public VerifyCodeVo(String smsId) {
		this.smsId = smsId;
	}

	public String getSmsId() {
		return smsId;
	}

	public void setSmsId(String smsId) {
		this.smsId = smsId;
	}

}
