package com.bee.app.vo;

import java.util.List;

/**
 * 快递查询接口返回对象,带注解的不返回
 *
 * @author liuzy
 * @date 2015年6月5日
 */
public class ExpressQueryVo extends BaseVo {
	private String message = "服务调用失败";

	private String mailNo;

	private String expTextName;

	private String tel;

	private List<Data> data;

	private transient String status;

	private String errCode;

	private transient String html;

	private transient String expSpellName;

	private transient String update;

	private transient String cache;

	private transient String ord;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		if ("".equals(message)) {
			this.message = "请求成功";
		} else {
			this.message = message;
		}
	}

	public String getMailNo() {
		return mailNo;
	}

	public void setMailNo(String mailNo) {
		this.mailNo = mailNo;
	}

	public String getExpTextName() {
		return expTextName;
	}

	public void setExpTextName(String expTextName) {
		this.expTextName = expTextName;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public List<Data> getData() {
		return data;
	}

	public void setData(List<Data> data) {
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public String getExpSpellName() {
		return expSpellName;
	}

	public void setExpSpellName(String expSpellName) {
		this.expSpellName = expSpellName;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public String getCache() {
		return cache;
	}

	public void setCache(String cache) {
		this.cache = cache;
	}

	public String getOrd() {
		return ord;
	}

	public void setOrd(String ord) {
		this.ord = ord;
	}

}

class Data {
	private String time;
	private String context;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}
}
