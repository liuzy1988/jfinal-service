package com.bee.app.vo;

import java.util.List;

import com.bee.app.vo.bean.AreaBean;

/**
 * 服务区域接口返回对象
 *
 * @author liuzy
 * @date 2015年6月5日
 */
public class PublicAreaVo extends BaseVo {

	private List<AreaBean> areaList;

	public List<AreaBean> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<AreaBean> areaList) {
		this.areaList = areaList;
	}

}
