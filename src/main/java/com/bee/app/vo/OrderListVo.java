package com.bee.app.vo;

import java.util.List;

import com.bee.app.vo.bean.OrderBean;

/**
 * 订单查询返回对象
 *
 * @author liuzy
 * @date 2015年6月11日
 */
public class OrderListVo extends BaseVo {
	private int orderCount;
	private List<OrderBean> orderList;

	public int getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(int orderCount) {
		this.orderCount = orderCount;
	}

	public List<OrderBean> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<OrderBean> orderList) {
		this.orderList = orderList;
	}
}
