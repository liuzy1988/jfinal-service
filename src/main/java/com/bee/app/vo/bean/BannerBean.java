package com.bee.app.vo.bean;

/**
 * 首页广告
 *
 * @author liuzy
 */
public class BannerBean {
	private String title;
	private String imgUrl;
	private String href;

	public BannerBean(String title, String imgUrl, String href) {
		this.title = title;
		this.imgUrl = imgUrl;
		this.href = href;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}
}
