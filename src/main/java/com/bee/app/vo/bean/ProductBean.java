package com.bee.app.vo.bean;

import java.math.BigDecimal;

/**
 * 商品(衣物)
 *
 * @author liuzy
 * @date 2015年6月6日
 */
public class ProductBean {
	private String type;
	private String name;
	private BigDecimal price;
	private int count;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
