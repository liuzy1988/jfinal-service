package com.bee.app.vo;

import java.util.List;
import java.util.Map;

/**
 * 更新接口返回对象
 *
 * @author liuzy
 */
public class PublicUpdateVo extends BaseVo {
	private String latesVesion;
	private String downloadUrl;
	private String secret;
	private List<String> codeList;
	private Map<String, String> codeMap;

	public String getLatesVesion() {
		return latesVesion;
	}

	public void setLatesVesion(String latesVesion) {
		this.latesVesion = latesVesion;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public List<String> getCodeList() {
		return codeList;
	}

	public void setCodeList(List<String> codeList) {
		this.codeList = codeList;
	}

	public Map<String, String> getCodeMap() {
		return codeMap;
	}

	public void setCodeMap(Map<String, String> codeMap) {
		this.codeMap = codeMap;
	}
}
