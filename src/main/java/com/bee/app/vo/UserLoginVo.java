package com.bee.app.vo;

/**
 * 用户登陆返回对象
 *
 * @author liuzy
 * @date 2015年6月5日
 */
public class UserLoginVo extends BaseVo {

	private String userId;
	
	private String areaId;
	
    private String token;

    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
