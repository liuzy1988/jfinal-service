package com.bee.app.vo;

/**
 * 创建订单返回对象
 *
 * @author liuzy
 * @date 2015年6月6日
 */
public class OrderCreateVo extends BaseVo {
	private String orderId;
	private transient String orderStatus;
	private transient String payStatus;
	private transient String refundStatus;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}
}
