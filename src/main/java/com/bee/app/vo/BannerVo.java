package com.bee.app.vo;

import java.util.List;

import com.bee.app.vo.bean.BannerBean;

public class BannerVo extends BaseVo {
	private List<BannerBean> bannerList;

	public List<BannerBean> getBannerList() {
		return bannerList;
	}

	public void setBannerList(List<BannerBean> bannerList) {
		this.bannerList = bannerList;
	}
}
