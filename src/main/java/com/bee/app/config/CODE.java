package com.bee.app.config;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 编码表
 *
 * @author liuzy
 * @date 2015年6月7日
 */
public class CODE {
	/**
	 * ERROR_服务器异常
	 */
	public static String ERROR = "ERROR";
	public static String ERROR_MSG = "ERROR_服务器异常";
	/**
	 * E999_签名错误
	 */
	public static String E999 = "E999";
	public static String E999_MSG = "E999_签名错误";
	/**
	 * E000_参数错误
	 */
	public static String E000 = "E000";
	public static String E000_MSG = "E000_参数错误";
	/**
	 * E001_用户名已存在
	 */
	public static String E001 = "E001";
	public static String E001_MSG = "E001_用户名已存在";
	/**
	 * E002_手机号已存在
	 */
	public static String E002 = "E002";
	public static String E002_MSG = "E002_手机号已存在";
	/**
	 * E003验证码错误或已失效
	 */
	public static String E003 = "E003";
	public static String E003_MSG = "E003_验证码错误或已失效";
	/**
	 * E004_用户名或密码错误
	 */
	public static String E004 = "E004";
	public static String E004_MSG = "E004_用户名或密码错误";
	/**
	 * E005_令牌无效
	 */
	public static String E005 = "E005";
	public static String E005_MSG = "E005_令牌无效";
	/**
	 * E006_终端类型不存在
	 */
	public static String E006 = "E006";
	public static String E006_MSG = "E006_终端类型不存在";
	/**
	 * E007_版本无效
	 */
	public static String E007 = "E007";
	public static String E007_MSG = "E007_版本无效";
	/**
	 * E008_快递单号无效
	 */
	public static String E008 = "E008";
	public static String E008_MSG = "E008_快递单号无效";
	/**
	 * E009_订单不存在
	 */
	public static String E009 = "E009";
	public static String E009_MSG = "E009_订单不存在";
	/**
	 * E010_订单已支付
	 */
	public static String E010 = "E010";
	public static String E010_MSG = "E010_订单已支付";
	/**
	 * E011_订单已取消
	 */
	public static String E011 = "E011";
	public static String E011_MSG = "E011_订单已取消";

	// -----------------------------------------------------------------

	/**
	 * OT01_快递订单
	 */
	public static String OT01 = "OT01";
	public static String OT01_MSG = "OT01_快递订单";
	/**
	 * OT02_干洗订单
	 */
	public static String OT02 = "OT02";
	public static String OT02_MSG = "OT02_干洗订单";
	/**
	 * OS00_下单成功
	 */
	public static String OS00 = "OS00";
	public static String OS00_MSG = "OS00_下单成功";
	/**
	 * OS01_已取消
	 */
	public static String OS01 = "OS01";
	public static String OS01_MSG = "OS01_已取消";
	/**
	 * OS02_已接单
	 */
	public static String OS02 = "OS02";
	public static String OS02_MSG = "OS02_已接单";
	/**
	 * OS03_已更新
	 */
	public static String OS03 = "OS03";
	public static String OS03_MSG = "OS03_已更新";
	/**
	 * OS04_已收件
	 */
	public static String OS04 = "OS04";
	public static String OS04_MSG = "OS04_已收件";
	/**
	 * OS05_干洗完成，待出库
	 */
	public static String OS05 = "OS05";
	public static String OS05_MSG = "OS05_干洗完成，待出库";
	/**
	 * OS06_已出库，运输途中
	 */
	public static String OS06 = "OS06";
	public static String OS06_MSG = "OS06_已出库，运输途中";
	/**
	 * OS07_快递已发出
	 */
	public static String OS07 = "OS07";
	public static String OS07_MSG = "OS07_快递已发出";
	/**
	 * OS08_派送中
	 */
	public static String OS08 = "OS08";
	public static String OS08_MSG = "OS08_派送中";
	/**
	 * OS09_订单完成
	 */
	public static String OS09 = "OS09";
	public static String OS09_MSG = "OS09_订单完成";
	/**
	 * PC01_现金支付
	 */
	public static String PC01 = "PC01";
	public static String PC01_MSG = "PC01_现金支付";
	/**
	 * PC02_支付宝支付
	 */
	public static String PC02 = "PC02";
	public static String PC02_MSG = "PC02_支付宝支付";
	/**
	 * PS00_未支付
	 */
	public static String PS00 = "PS00";
	public static String PS00_MSG = "PS00_未支付";
	/**
	 * PS01_正在支付
	 */
	public static String PS01 = "PS01";
	public static String PS01_MSG = "PS01_正在支付";
	/**
	 * PS02_支付失败/重新支付
	 */
	public static String PS02 = "PS02";
	public static String PS02_MSG = "PS02_支付失败/重新支付";
	/**
	 * PS03_支付状态不确认
	 */
	public static String PS03 = "PS03";
	public static String PS03_MSG = "PS03_支付状态不确认";
	/**
	 * PS04_支付成功/已支付
	 */
	public static String PS04 = "PS04";
	public static String PS04_MSG = "PS04_支付成功/已支付";
	/**
	 * RS00_无退款
	 */
	public static String RS00 = "RS00";
	public static String RS00_MSG = "RS00_无退款";
	/**
	 * RS01_申请退款
	 */
	public static String RS01 = "RS01";
	public static String RS01_MSG = "RS01_申请退款";
	/**
	 * RS02_退款处理
	 */
	public static String RS02 = "RS02";
	public static String RS02_MSG = "RS02_退款处理";
	/**
	 * RS03_退款成功/已退款
	 */
	public static String RS03 = "RS03";
	public static String RS03_MSG = "RS03_退款成功/已退款";
	/**
	 * RS04_退款完成
	 */
	public static String RS04 = "RS04";
	public static String RS04_MSG = "RS04_退款完成";
	/**
	 * PT01_上衣类
	 */
	public static String PC03 = "PC03";
	public static String PC03_MSG = "PC03_微信支付";
	/**
	 * PT01_上衣类
	 */
	public static String PT01 = "PT01";
	public static String PT01_MSG = "PT01_上衣类";
	/**
	 * PT02_下装类
	 */
	public static String PT02 = "PT02";
	public static String PT02_MSG = "PT02_下装类";
	/**
	 * PT03_家居类
	 */
	public static String PT03 = "PT03";
	public static String PT03_MSG = "PT03_家居类";

	public static List<String> toList() {
		List<String> values = new ArrayList<>();
		Field[] fields = CODE.class.getDeclaredFields();
		for (Field field : fields) {
			try {
				if (field.getName().contains("_")) {
					values.add(field.get(null).toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return values;
	}

	public static Map<String, String> toMap() {
		Map<String, String> map = new HashMap<>();
		Field[] fields = CODE.class.getDeclaredFields();
		for (Field field : fields) {
			try {
				if (field.getName().contains("_")) {
					String value = field.get(null).toString();
					map.put(value.split("_")[0], value.split("_")[1]);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return map;
	}

}
