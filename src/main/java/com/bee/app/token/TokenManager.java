package com.bee.app.token;

import com.bee.app.util.AES;
import com.bee.app.util.StringKit;
import com.jfinal.plugin.ehcache.CacheKit;

/**
 * 用户令牌管理
 * 
 * @author liuzy
 * @since 2015年7月2日
 */
public class TokenManager {

	/**
	 * Token缓存名
	 */
	private static String TOKEN = "Token";

	/**
	 * Token加密密钥
	 */
	private static String secretKey = "liuzy";

	/**
	 * 重新生成用户Token,用户旧Token过期
	 *
	 * @param token
	 * @return
	 */
	public static String createToken(String userId) {
		if (CacheKit.get(TOKEN, userId) != null) {
			CacheKit.remove(TOKEN, userId);
		}
		String token = AES.encrypt(userId + System.currentTimeMillis(), secretKey);
		CacheKit.put(TOKEN, userId, token);
		return token;
	}

	/**
	 * 取得用户Token对象
	 *
	 * @param userId
	 * @return
	 */
	public static String getToken(String userId) {
		return CacheKit.get(TOKEN, userId);
	}

	/**
	 * 验证Token有效性
	 *
	 * @param token
	 * @return
	 */
	public static boolean isToken(String token) {
		if (StringKit.isNullOrEmpty(token)) {
			return false;
		}
		String userId = token2userId(token);
		if (userId != null) {
			String t = CacheKit.get(TOKEN, userId);
			if (t != null && t.equals(token)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 删除用户Token,使其失效
	 *
	 * @param token
	 */
	public static void removeToken(String token) {
		CacheKit.remove(TOKEN, token2userId(token));
	}

	/**
	 * 从Token中解密出用户ID
	 *
	 * @param token
	 * @return
	 */
	public static String token2userId(String token) {
		try {
			return token.length() == 96 ? AES.decrypt(token, secretKey).substring(0, 32) : null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
