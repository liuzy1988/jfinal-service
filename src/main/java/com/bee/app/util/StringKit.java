package com.bee.app.util;

import java.math.BigInteger;
import java.util.regex.Pattern;

/**
 * 字符串处理工具类
 *
 * @author liuzy
 * @date 2015年6月7日
 */
public class StringKit {

	public static boolean isNull(String string) {
		return string == null;
	}

	public static boolean hasNull(String... string) {
		for (String str : string) {
			if (isNull(str)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isNullOrEmpty(String string) {
		return string == null || "".equals(string);
	}

	public static boolean hasNullOrEmpty(String... string) {
		for (String str : string) {
			if (isNullOrEmpty(str)) {
				return true;
			}
		}
		return false;
	}

	public static String bytesToHexStr(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		if (bytes == null || bytes.length == 0) {
			return null;
		}
		for (int i = 0; i < bytes.length; i++) {
			String hv = Integer.toHexString(bytes[i] & 0xFF);
			if (hv.length() == 1) {
				sb.append(0);
			}
			sb.append(hv);
		}
		return sb.toString();
	}

	public static byte[] hexStrToBytes(String hexString) {
		String hex = "0123456789ABCDEF";
		if (hexString == null || "".equals(hexString)) {
			return null;
		}
		int length = hexString.length() / 2;
		char[] hexChars = hexString.toUpperCase().toCharArray();
		byte[] bytes = new byte[length];
		for (int i = 0; i < length; i++) {
			int pos = i * 2;
			bytes[i] = (byte) (hex.indexOf(hexChars[pos]) << 4 | hex.indexOf(hexChars[pos + 1]));

		}
		return bytes;
	}

	public static byte[] fromHex(String hex) {
		byte[] bytes = new byte[hex.length() / 2];
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return bytes;
	}

	public static String toHex(byte[] bytes) {
		BigInteger bi = new BigInteger(1, bytes);
		String hex = bi.toString(16);
		int paddingLength = (bytes.length * 2) - hex.length();
		if (paddingLength > 0)
			return String.format("%0" + paddingLength + "d", 0) + hex;
		else
			return hex;
	}

	public static boolean isPhoneNumber(String userName) {
		if (isNullOrEmpty(userName))
			return false;
		else
			return Pattern.matches("^[1][3,4,5,7,8][0-9]{9}$", userName);
	}

	public static boolean isUserName(String userName) {
		if (isNullOrEmpty(userName))
			return false;
		else
			return Pattern.matches("([a-zA-Z][a-zA-Z0-9_]{4,15})|[\\u4e00-\\u9fa5]{2,5}", userName);
	}

	public static boolean isBlank(String str) {
		return str == null || "".equals(str.trim()) ? true : false;
	}

	public static boolean notBlank(String str) {
		return str == null || "".equals(str.trim()) ? false : true;
	}

	public static boolean notBlank(String... strings) {
		if (strings == null)
			return false;
		for (String str : strings)
			if (str == null || "".equals(str.trim()))
				return false;
		return true;
	}

	public static boolean notBlank(StringBuffer sb) {
		return notBlank(sb.toString());
	}

	public static boolean notNull(String[] array) {
		return array != null;
	}
	
	public static boolean notNull(Object... paras) {
		if (paras == null)
			return false;
		for (Object obj : paras)
			if (obj == null)
				return false;
		return true;
	}

}
