package com.bee.app.util;

import java.util.UUID;

public class IDUtil {

	public static void main(String[] args) {
		System.out.println(IDUtil.rand());
	}

	public static String rand() {
		return UUID.randomUUID().toString().replace("-", "");
	}

}
