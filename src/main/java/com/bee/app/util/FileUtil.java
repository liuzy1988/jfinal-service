package com.bee.app.util;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * 文件操作工具
 * 
 * @author liuzy
 * @since 2015年5月22日
 */
public class FileUtil {

	/**
	 * 写入文件
	 *
	 * @param keyBytes
	 * @param filePath
	 */
	public static void writeFile(byte[] keyBytes, String filePath) {
		try {
			FileOutputStream fos = new FileOutputStream(filePath);
			fos.write(keyBytes);
			fos.flush();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 读取文件
	 *
	 * @param filePath
	 * @return
	 */
	public static byte[] readFile(String filePath) {
		byte[] keyBytes = null;
		try {
			File file = new File(filePath);
			FileInputStream fis = new FileInputStream(file);
			DataInputStream dis = new DataInputStream(fis);
			keyBytes = new byte[(int) file.length()];
			dis.readFully(keyBytes);
			dis.close();
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return keyBytes;
	}

}
