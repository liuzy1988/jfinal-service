package com.bee.app.util;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * FastJson公具类
 *
 * @author liuzy
 * @date 2015年6月6日
 */
public class FastJsonUtil {

	private static SerializerFeature[] serializerFeatures = { SerializerFeature.WriteMapNullValue, SerializerFeature.WriteNullNumberAsZero, SerializerFeature.WriteNullBooleanAsFalse,
			SerializerFeature.WriteNullStringAsEmpty };

	public static String toJSON(Object o) {
		return JSON.toJSONString(o, serializerFeatures);
	}

	public static <T> T fromJSON(String json, Class<T> clazz) {
		try {
			return JSON.parseObject(json, clazz);
		} catch (Exception e) {
			return null;
		}
	}

	public static List<?> fromArrayJSON(String json, Class<?> clazz) {
		try {
			return JSON.parseArray(json, clazz);
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}

}