package com.bee.app.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 * HttpClient查快递
 *
 * @author liuzy
 * @date 2015年6月4日
 */
public class HttpClientUtil {

	private static CloseableHttpClient httpClient = HttpClients.createDefault();

	/**
	 * GET请求
	 * 
	 * @param url
	 * @param params
	 * @return
	 */
	public static String doGET(String url, Map<String, String> params) {
		url += "?";
		for (String key : params.keySet()) {
			url += "&" + key + "=" + params.get(key);
		}
		HttpGet httpGet = new HttpGet(url);
		return execute(httpGet);
	}

	/**
	 * POST请求
	 * 
	 * @param url
	 * @param params
	 * @return
	 */
	public static String doPOST(String url, Map<String, String> params) {
		HttpPost httpPost = new HttpPost(url);
		if (params != null)
			addParams(httpPost, params);
		return execute(httpPost);
	}

	/**
	 * POST请求
	 * 
	 * @param url
	 * @param params
	 * @param headers
	 * @return
	 */
	public static String doPOST(String url, Map<String, String> params, Map<String, String> headers) {
		HttpPost httpPost = new HttpPost(url);
		if (params != null)
			addParams(httpPost, params);
		if (headers != null)
			addHeaders(httpPost, headers);
		return execute(httpPost);
	}

	/**
	 * POST请求
	 * 
	 * @param url
	 * @param json
	 * @param headers
	 * @return
	 */
	public static String doPOST(String url, String json, Map<String, String> headers) {
		HttpPost httpPost = new HttpPost(url);
		if (json != null)
			httpPost.setEntity(EntityBuilder.create().setText(json).build());
		if (headers != null)
			addHeaders(httpPost, headers);
		return execute(httpPost);
	}

	private static void addParams(HttpPost httpPost, Map<String, String> params) {
		List<NameValuePair> nvp = new ArrayList<NameValuePair>();
		for (String key : params.keySet()) {
			nvp.add(new BasicNameValuePair(key, params.get(key)));
		}
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nvp, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	private static void addHeaders(HttpPost httpPost, Map<String, String> headers) {
		for (String key : headers.keySet()) {
			httpPost.addHeader(new BasicHeader(key, headers.get(key)));
		}
	}

	/**
	 * 执行请求
	 *
	 * @param httpRequest
	 *            HttpClient执行的httpRequest
	 * @return 请求的结果内容
	 */
	private static String execute(HttpRequestBase httpRequest) {
		try {
			HttpResponse httpResponse = httpClient.execute(httpRequest);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				return EntityUtils.toString(httpResponse.getEntity(), "GBK");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
