package com.bee.app.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static String format(long date) {
		return simpleDateFormat.format(new Date(date));
	}

	public static String format(Date date) {
		return simpleDateFormat.format(date);
	}

	public static String getDate(String pattern) {
		return new SimpleDateFormat(pattern).format(new Date());
	}

	public static String getDate(String pattern, Date date) {
		return new SimpleDateFormat(pattern).format(date);
	}

}
