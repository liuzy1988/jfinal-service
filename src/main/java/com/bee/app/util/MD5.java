package com.bee.app.util;

import java.security.MessageDigest;

public class MD5 {
	public static String md5(String s) {
		try {
			byte[] btInput = s.getBytes();
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			byte[] md = mdInst.digest();
			return StringKit.bytesToHexStr(md);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
