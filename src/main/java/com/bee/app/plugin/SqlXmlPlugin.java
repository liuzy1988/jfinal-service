package com.bee.app.plugin;

import com.bee.app.tool.ToolSqlXml;
import com.jfinal.plugin.IPlugin;

/**
 * 加载sql文件
 * 
 * @author liuzy
 * @version 2015年7月4日
 */
public class SqlXmlPlugin implements IPlugin {

	public SqlXmlPlugin() {
	}

	@Override
	public boolean start() {
		ToolSqlXml.init(true);
		return true;
	}

	@Override
	public boolean stop() {
		ToolSqlXml.destory();
		return true;
	}

}
