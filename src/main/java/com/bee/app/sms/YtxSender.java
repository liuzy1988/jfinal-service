package com.bee.app.sms;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.bee.app.util.Base64;
import com.bee.app.util.DateUtil;
import com.bee.app.util.FastJsonUtil;
import com.bee.app.util.HttpClientUtil;
import com.bee.app.util.MD5;

/**
 * 云通讯短信验证码发送
 * 
 * @author liuzy
 */
public class YtxSender {
	
	private Logger logger = Logger.getLogger(YtxSender.class);

	private String account = "aaf98f894e2360b4014e308da1630c92";
	private String token = "eab63df474b34311b80be638013b3bfb";
	private String appId = "aaf98f894e2360b4014e308df4ea0c95";

	public String send(String phoneNumber, String content) {
		try {
			String url = "https://sandboxapp.cloopen.com:8883/2013-12-26/Accounts/"+this.account+"/SMS/TemplateSMS?sig=";
			String timestamp = DateUtil.getDate("yyyyMMddHHmmss");
			String sig = MD5.md5(this.account + this.token + timestamp);
			url += sig;
			String src = this.account + ":" + timestamp;
			String auth = Base64.encode(src.getBytes());
			
			Msg msg = new Msg(this.appId);
			msg.setTo(phoneNumber);
			msg.setTemplateId("1");
			msg.setDatas(content);
			String json = FastJsonUtil.toJSON(msg);
			
			Map<String, String> headers = new HashMap<>();
			headers.put("Authorization", auth);
			headers.put("Accept", "application/json");
			headers.put("Content-Type", "application/json;charset=utf-8");
			String response = HttpClientUtil.doPOST(url, json, headers);
			logger.info(response);
			Result result = FastJsonUtil.fromJSON(response, Result.class);
			return result.getTemplateSMS().getSmsMessageSid();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}
	
}
class Msg {
	private String appId;
	private String to;
	private String templateId;
	private String[] datas;
	public Msg(String appId) {
		this.appId = appId;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String[] getDatas() {
		return datas;
	}
	public void setDatas(String[] datas) {
		this.datas = datas;
	}
	public void setDatas(String content) {
		String[] s = {content, "1"};
		this.datas = s;
	}
}
class Result {
	private String statusCode;
	private String statusMsg;
	private TemplateSMS templateSMS;
	class TemplateSMS {
		private String dateCreated;
		private String smsMessageSid;
		public String getDateCreated() {
			return dateCreated;
		}
		public void setDateCreated(String dateCreated) {
			this.dateCreated = dateCreated;
		}
		public String getSmsMessageSid() {
			return smsMessageSid;
		}
		public void setSmsMessageSid(String smsMessageSid) {
			this.smsMessageSid = smsMessageSid;
		}
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMsg() {
		return statusMsg;
	}
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}
	public TemplateSMS getTemplateSMS() {
		return templateSMS;
	}
	public void setTemplateSMS(TemplateSMS templateSMS) {
		this.templateSMS = templateSMS;
	}
}
