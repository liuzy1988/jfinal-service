package com.bee.app.express;

import java.util.HashMap;
import java.util.Map;

import com.bee.app.util.HttpClientUtil;

/**
 * 小米五八查快递
 *
 * @author liuzy
 * @version 2015年6月27日
 */
public class XM58Express {

	//private static final String url = "http://www.xiaomi58.com/kd/do.php";
	private static final String url = "http://23.226.129.112/kd/do.php";
	
	public static String query(String expressNumber) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("mailNo", expressNumber);
		params.put("com", "");
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Referer", "http://www.xiaomi58.com/kd/zidongcha.php");
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		headers.put("X-Requested-With", "XMLHttpRequest");
		headers.put("Accept", "text/html, */*");
		headers.put("Accept-Language", "en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3");
		headers.put("Accept-Encoding", "gzip, deflate");
		headers.put("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko");
		headers.put("Host", "www.xiaomi58.com");
		headers.put("Connection", "Keep-Alive");
		headers.put("Cache-Control", "no-cache");
		headers.put("Cookie", "AJSTAT_ok_pages=4; AJSTAT_ok_times=2");
		try {
			String result = HttpClientUtil.doPOST(url, params, headers);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
