package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
@Table("bee_order")
public class Order extends Model<Order> {

	private static final long serialVersionUID = -572778906265137673L;

	public static Order dao = new Order();

}
