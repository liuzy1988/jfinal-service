package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
@Table("bee_express_cache")
public class ExpressCache extends Model<ExpressCache> {

	private static final long serialVersionUID = 1650639164238354925L;

	public static final ExpressCache dao = new ExpressCache();

}
