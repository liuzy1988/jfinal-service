package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
@Table("bee_sale_area")
public class SaleArea extends Model<SaleArea> {

	private static final long serialVersionUID = 1411786420189048701L;

	public SaleArea dao = new SaleArea();

}
