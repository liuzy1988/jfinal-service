package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @version 2015年7月5日
 */
@Table("bee_sugesstion")
public class Sugesstion extends Model<Sugesstion> {

	private static final long serialVersionUID = 7015691740112704976L;

	public static final Sugesstion dao = new Sugesstion();

}
