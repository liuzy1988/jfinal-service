package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
@Table("bee_order_detail")
public class OrderDetail extends Model<OrderDetail> {

	private static final long serialVersionUID = 6062049970448854570L;

	public static OrderDetail dao = new OrderDetail();

}
