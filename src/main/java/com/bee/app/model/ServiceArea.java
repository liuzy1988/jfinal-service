package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
@Table("bee_service_area")
public class ServiceArea extends Model<ServiceArea> {

	private static final long serialVersionUID = -4224464087201988777L;

	public static ServiceArea dao = new ServiceArea();

}
