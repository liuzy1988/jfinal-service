package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
@Table("bee_product_type")
public class ProductType extends Model<ProductType> {

	private static final long serialVersionUID = 5562827906595519880L;

	public ProductType dao = new ProductType();

}
