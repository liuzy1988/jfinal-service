package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @version 2015年7月5日
 */
@Table("bee_banner")
public class Banner extends Model<Banner> {

	private static final long serialVersionUID = 8881713500837527983L;

	public static final Banner dao = new Banner();

}
