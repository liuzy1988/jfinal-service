package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
@Table("bee_user")
public class User extends Model<User> {

	private static final long serialVersionUID = 1977033733415926657L;

	public static User dao = new User();

}
