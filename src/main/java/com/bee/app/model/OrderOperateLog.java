package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
@Table("bee_order_operate_log")
public class OrderOperateLog extends Model<OrderOperateLog> {

	private static final long serialVersionUID = 680677044201737358L;

	public OrderOperateLog dao = new OrderOperateLog();

}
