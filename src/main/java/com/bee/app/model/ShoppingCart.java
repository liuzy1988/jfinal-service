package com.bee.app.model;

import com.bee.app.jfinal.Table;
import com.jfinal.plugin.activerecord.Model;

/**
 * 
 * @author liuzy
 * @since 2015年7月3日
 */
@Table("bee_shopping_cart")
public class ShoppingCart extends Model<ShoppingCart> {

	private static final long serialVersionUID = 6715397630978434848L;

	public ShoppingCart dao = new ShoppingCart();

}
