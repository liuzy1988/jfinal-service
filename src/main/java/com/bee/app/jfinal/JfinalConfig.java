package com.bee.app.jfinal;

import com.bee.app.plugin.LoadModels;
import com.bee.app.plugin.LoadRoutes;
import com.bee.app.plugin.SqlXmlPlugin;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;

/**
 * 
 * @author liuzy
 * @version 2015年7月2日
 */
public class JfinalConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		PropKit.use("project.properties");
		me.setEncoding("UTF-8");
		me.setViewType(ViewType.JSP);
		me.setDevMode(PropKit.getBoolean("jfinal.devmode", false));
		me.setError404View("/error/404.html");
		me.setError500View("/error/500.html");
	}

	@Override
	public void configHandler(Handlers me) {

	}

	@Override
	public void configInterceptor(Interceptors me) {

	}

	@Override
	public void configPlugin(Plugins me) {
		// druid
		DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("druid.jdbcUrl"), PropKit.get("druid.username"), PropKit.get("druid.password"));
		druidPlugin.set(PropKit.getInt("druid.initialSize"), PropKit.getInt("druid.minIdle"), PropKit.getInt("druid.maxActive"));
		druidPlugin.setTimeBetweenEvictionRunsMillis(PropKit.getInt("druid.timeBetweenEvictionRunsMillis"));
		druidPlugin.setMinEvictableIdleTimeMillis(PropKit.getInt("druid.minEvictableIdleTimeMillis"));
		druidPlugin.setFilters(PropKit.get("druid.filters"));
		druidPlugin.setValidationQuery(PropKit.get("druid.validationQuery"));
		druidPlugin.setTestWhileIdle(PropKit.getBoolean("druid.testWhileIdle"));
		druidPlugin.setTestOnBorrow(PropKit.getBoolean("druid.testOnBorrow"));
		druidPlugin.setTestOnReturn(PropKit.getBoolean("druid.testOnReturn"));
		me.add(druidPlugin);
		// arp + model
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		arp.setDevMode(PropKit.getBoolean("jfinal.devmode", false));
		arp.setShowSql(PropKit.getBoolean("jfinal.devmode", false));
		arp.setDialect(new MysqlDialect());
		arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
		new LoadModels(arp).scan();
		me.add(arp);
		// sqlxml
		me.add(new SqlXmlPlugin());
		// ehcache
		me.add(new EhCachePlugin());
	}

	@Override
	public void configRoute(Routes me) {
		new LoadRoutes(me).scan();
	}

}
