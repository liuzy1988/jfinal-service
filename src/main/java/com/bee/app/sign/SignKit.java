package com.bee.app.sign;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.bee.app.util.MD5;

/**
 * 签名验签工具
 *
 * @author liuzy
 */
public class SignKit {

	public static final String SIGNKEY = "signature";

	/**
	 * 验签
	 *
	 * @param params
	 * @return
	 */
	public static boolean isSign(Map<String, String> params) {
		String signed = params.get(SIGNKEY);
		if (signed == null) {
			return false;
		}
		return true;
		// return signed.equals(sign(params));
	}

	/**
	 * 签名
	 *
	 * @param params
	 * @return
	 */
	public static String sign(Map<String, String> params) {
		SortedMap<String, String> sortedMap = new TreeMap<String, String>();
		sortedMap.putAll(params);
		sortedMap.remove(SIGNKEY);
		StringBuilder sb = new StringBuilder();
		for (String key : sortedMap.keySet()) {
			sb.append(key).append("=").append(sortedMap.get(key)).append("&");
		}
		String str = sb.length() > 0 ? sb.toString().substring(0, sb.toString().length() - 1) : "";
		return MD5.md5(str);
	}
}
